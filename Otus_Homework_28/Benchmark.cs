﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_28
{
    class Benchmark
    {        public static TimeSpan DoBenchmark(Action action)
        {
            var sw = new Stopwatch();
            sw.Start();

            action();

            sw.Stop();

            return sw.Elapsed;
        }
    }
}
