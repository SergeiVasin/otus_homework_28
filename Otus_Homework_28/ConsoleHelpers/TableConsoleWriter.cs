﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_28.ConsoleHelpers
{
    public class TableConsoleWriter<TData>
    {
        public void WriteTable(IEnumerable<TData> data)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.DarkGreen;

            PropertyInfo[] propertyInfos = typeof(TData).GetProperties();
            var propertyNames = propertyInfos.Select(propInfo => propInfo.Name).ToArray();

            ConsoleTableBuilder.WriteSeperatorLine();
            ConsoleTableBuilder.WriteRow(propertyNames);
            ConsoleTableBuilder.WriteSeperatorLine();

            foreach (var datum in data)
            {
                var values = propertyInfos.Select(propInfo => propInfo?.GetValue(datum, null)?.ToString()).ToArray();
                ConsoleTableBuilder.WriteRow(values);
            }

            ConsoleTableBuilder.WriteSeperatorLine();

            Console.ResetColor();
        }
    }
}
