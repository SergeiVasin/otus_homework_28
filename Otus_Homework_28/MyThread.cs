﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_28
{
    public class MyThread
    {
        private readonly int[] _sequence;
        private readonly int _startInd;
        private readonly int _finishInd;
        public Thread Thread { get; private set; }
        public long SumResult { get; private set; }

        public MyThread(int[] sequence, int startInd, int finishInd, CountdownEvent countdown )
        {
            _sequence = sequence;
            _startInd = startInd;
            _finishInd = finishInd;
            Thread = new Thread(Sum);
            Thread.Start(countdown);
        }

        void Sum(object? obj)
        {
            for (int i = _startInd; i <= _finishInd; i++)
            {
                SumResult += _sequence[i];
            }
            ((CountdownEvent)obj).Signal();
        }
    }
}
