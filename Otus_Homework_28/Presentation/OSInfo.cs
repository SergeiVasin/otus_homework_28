﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_28
{
    public class OSInfo
    {
        public string? OSVersion { get; private set; }
        public string? ProcessorArchitecture { get; private set; }
        public string? ProcessorModel { get; private set; }
        public int ProcessorCount { get; private set; }
        public string? CoreFrequency { get; private set; }
        public string? TotalMemory { get; private set; }
        public string? TotalVirtualMemory { get; private set; }
        public string? AvailableMemory { get; private set; }
        public string? AvailableVirtualMemory { get; private set; }

        public OSInfo()
        {
            OSVersion = Environment.OSVersion.VersionString;
            ProcessorArchitecture = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            ProcessorModel = Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER");
            ProcessorCount = Environment.ProcessorCount;

            var searcherProc = new ManagementObjectSearcher(
            "select * from Win32_Processor");

            foreach (var item in searcherProc.Get())
            {
                CoreFrequency = item["MaxClockSpeed"].ToString();
            }

            var searcherMem = new ManagementObjectSearcher(
            "select * from CIM_OperatingSystem");

            foreach (var item in searcherMem.Get())
            {
                TotalMemory = item["TotalVisibleMemorySize"].ToString();
                TotalVirtualMemory = item["TotalVirtualMemorySize"].ToString();
                AvailableMemory = item["FreePhysicalMemory"].ToString();
                AvailableVirtualMemory = item["FreeVirtualMemory"].ToString();
            }

        }
        public override string ToString()
        {
            StringBuilder systemInfo = new StringBuilder("");

            systemInfo.AppendFormat("Operation system:  {0}\r\n", OSVersion);
            systemInfo.AppendFormat("Processor srchitecture:  {0}\r\n", ProcessorArchitecture);
            systemInfo.AppendFormat("Processor model:  {0}\r\n", ProcessorModel);
            systemInfo.AppendFormat("Processor count:  {0}\r\n", ProcessorCount);
            systemInfo.AppendFormat("Core frequency:  {0}\r\n", CoreFrequency);
            systemInfo.AppendFormat("Total physical memory:  {0}\r\n", TotalMemory);
            systemInfo.AppendFormat("Total virtual memory:  {0}\r\n", TotalVirtualMemory);
            systemInfo.AppendFormat("Available physical memory: {0}\r\n", AvailableMemory);
            systemInfo.AppendFormat("Available virtual memory: {0}", AvailableVirtualMemory);

            return systemInfo.ToString();
        }
    }
}
