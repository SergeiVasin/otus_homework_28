﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_28
{
    public class TableItem
    {
        public string Name { get; set; }
        public long Records { get; set; }
        public long Sum { get; set; }
        public TimeSpan TimeSpan { get; set; }
    }
}
