﻿

using Otus_Homework_28.ConsoleHelpers;
using System.Diagnostics;
using System.Linq;

namespace Otus_Homework_28
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine();
            Console.WriteLine("Окружение (характеристики компьютера и ОС):");
            Console.WriteLine("-------------------------------------------");

            var sysInfo = new OSInfo();

            Console.WriteLine(sysInfo.ToString());
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine();

            var sequence100_000 = Enumerable.Range(1, (int)Math.Pow(10, 5)).ToArray();
            var sequence1000_000 = Enumerable.Range(1, (int)Math.Pow(10, 6)).ToArray();
            var sequence10_000_000 = Enumerable.Range(1, (int)Math.Pow(10, 7)).ToArray();
            var sequence100_000_000 = Enumerable.Range(1, (int)Math.Pow(10, 8)).ToArray();
            var sequence1000_000_000 = Enumerable.Range(1, (int)Math.Pow(10, 9)).ToArray();

            DoAll(sequence100_000);
            DoAll(sequence1000_000);
            DoAll(sequence10_000_000);
            DoAll(sequence100_000_000);
            DoAll(sequence1000_000_000);

        }

        static void DoAll(int[] sequence)
        {
            Table table = new Table();
            table.TableItems.Add(DoNormal(sequence));
            table.TableItems.Add(DoWithThread(sequence));
            table.TableItems.Add(DoWithPlinq(sequence));

            var clientTableConsoleWriter = new TableConsoleWriter<TableItem>();
            clientTableConsoleWriter.WriteTable(table.TableItems);
        }

        static TableItem DoNormal(int[] sequence)
        {
            TableItem tableItem = new TableItem();
            tableItem.Name = "Normal counting";
            tableItem.Records = sequence.Length;

            tableItem.TimeSpan = Benchmark.DoBenchmark(() =>
            {
                tableItem.Sum = sequence.Sum(x => (long)x);
            });
            return tableItem;
        }

        static TableItem DoWithThread(int[] sequence)
        {
            TableItem tableItem = new TableItem();
            tableItem.Name = "Thread counting";
            tableItem.Records = sequence.Length;

            tableItem.TimeSpan = Benchmark.DoBenchmark(() =>
            {
                List<MyThread> threads = new List<MyThread>();
                int cores = Environment.ProcessorCount;
                int range = sequence.Length / cores;
                long threadsSumResult = 0;
                int startInd = 0;

                var countDown = new CountdownEvent(1);

                for (int i = 0; i < cores; i++)
                {
                    countDown.AddCount();

                    if (i == cores - 1)
                    {
                        threads.Add(new MyThread(sequence, startInd, sequence.Length - 1, countDown));
                        break;
                    }
                    threads.Add(new MyThread(sequence, startInd, startInd - 1 + range, countDown));
                    startInd += range;

                }
                countDown.Signal();
                countDown.Wait();

                foreach (var thread in threads)
                {
                    threadsSumResult += thread.SumResult;
                }

                tableItem.Sum = threadsSumResult;
            });

            return tableItem;
        }

        static TableItem DoWithPlinq(int[] sequence)
        {
            TableItem tableItem = new TableItem();
            tableItem.Name = "PLinq counting";
            tableItem.Records = sequence.Length;

            tableItem.TimeSpan = Benchmark.DoBenchmark(() =>
            {
                tableItem.Sum = sequence.AsParallel().Sum(x => (long)x);
            });

            return tableItem;
        }
    }
}



